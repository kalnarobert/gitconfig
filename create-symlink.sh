#!/bin/bash

GITCONFIG=${BACKED_UP_HOME}config/git/

if [ -f ~/.gitconfig ]; then
  cp ~/.gitconfig ${GITCONFIG}/gitconfig-overwritten-backup
  echo "old gitconfig saved to ${GITCONFIG}gitconfig-overwritten-backup"
fi

ln -sf ${GITCONFIG}/gitconfig ~/.gitconfig


